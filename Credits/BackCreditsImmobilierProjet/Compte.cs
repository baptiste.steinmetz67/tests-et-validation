﻿using System;

namespace BackCreditsImmobilierProjet
{
    public class Compte
    {
        public double Taux { get; set; }
        public double Plafond { get; set; } = double.MaxValue;
        public double Duree { get; set; } = 0;
        public double Balance { get; set; } = 0;
        public double Abondement { get; private set; } = 0;
        public TypeAbondement _typeAbondement { get; set; }

        public virtual void Crediter(double montant)
        {
            Balance += montant;
        }
        public virtual void Abonder(double montantAbondement)
        {
            Abondement += montantAbondement;
        }

        public virtual double CalculInteret()
        {
            for (int i = 0; i < Duree; i++)
            {
                if (PlafondEstAtteint())
                {
                    Balance += (Plafond * (Taux / 100));
                }
                else
                {
                    MontantParAbondement();
                    Balance += (Balance * (Taux / 100));
                }
            }
            return Math.Round(Balance, 2);
        }


        public virtual double CalculPlafondAnnee(double montantAAtteindre)
        {
            if (Abondement == 0) return Duree;

            double annee = 0;
            while(Balance < montantAAtteindre)
            {
                if (PlafondEstAtteint())
                {
                    Balance += (Plafond * (Taux / 100));
                }
                else
                {
                    Balance = MontantParAbondement();
                    Balance += (Balance * (Taux / 100));
                }
                annee++;
            }

            return annee;
        }

        public virtual double MontantParAbondement()
        {
            switch (_typeAbondement)
            {
                case TypeAbondement.Mensuel:
                    Balance += Abondement * 12;
                    break;
                case TypeAbondement.Trimestriel:
                    Balance += Abondement * 4;
                    break;
                case TypeAbondement.Annuel:
                    Balance += Abondement;
                    break;
            }
            return Math.Round(Balance, 2);
        }

        public virtual bool PlafondEstAtteint()
        {
            return Balance >= Plafond;
        }
    }
}
