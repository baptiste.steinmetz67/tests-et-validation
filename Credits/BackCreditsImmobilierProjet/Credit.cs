﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackCreditsImmobilierProjet
{
    public class Credit : Compte
    {
        public double Capital { get; set; }

        // public double CapitalRestantDu { get; set; }? 
        protected double TauxNominal { get; set; }
        public double TauxAssurance { get; set; } = 0.003;
        public TypeTauxEmprunt _typeTauxEmprunt { get; set; } = TypeTauxEmprunt.Bon;
        
        public Credit(double capital) : base()
        {
            this.Capital = capital;
        }

        public double EstSportif()
        {
            return TauxAssurance-= 0.0005;
        }

        public double EstFumeur()
        {
            return TauxAssurance += 0.0015;
        }
        public double EstCardiaque()
        {
            return TauxAssurance += 0.003;
        }

        public double EstIngeInfo()
        {
            return TauxAssurance -= 0.0005;
        }

        public double EstPiloteChasse()
        {
            return TauxAssurance += 0.0015;
        }

        public double CalculMensualiteSansAssurance()
        {
            // changé ici 
            CalculTauxEmprunt();
            double tauxMensuel = TauxNominal / 12;
            double dureeMois = Duree * 12;
            double num = Capital * tauxMensuel;
            double denum = 1 - Math.Pow((1 + tauxMensuel), - dureeMois);
            return Math.Round(num / denum, 2) ;
        }

        public double CalculMensualiteTotale()
        {
            // si différé changé ici aussi 
            return Math.Round((CalculMensualiteAssurance() + CalculMensualiteSansAssurance()),2);
        }

        public double CalculMensualiteAssurance()
        {
            // à changer car assurance à remboursé sur montant capital total restant dû 
            return Math.Round((TauxAssurance * Capital) / (Duree * 12),2);
        }
        public double CalculMontantTotalAssurance()
        {
            return Math.Round((CalculMensualiteAssurance() * (Duree * 12)),2);
        }

        public double CalculMontantTotalInteret()
        {
            return Math.Round(CalculMensualiteTotale() * (Duree * 12) - Capital,2);
        }

        public double MontantRembourseApresDuree(int duree)
        {

            // si différé changé ici : durée du différé + remboursement intéret seulement ?
            if(duree < 0)
            {
                throw new ArgumentException("Duree ne peut être négative", nameof(duree));
            }
            double res = 0;
            for (int i = 0; i < duree; i++)
            {
                res += (CalculMensualiteTotale() * 12); 
            }
            return Math.Round(res,2);
        }

        public double CalculTauxEmprunt()
        {
            switch(_typeTauxEmprunt)
            {
                case TypeTauxEmprunt.Bon:
                    if(Duree >= 7 && Duree < 10)
                    {
                        TauxNominal = 0.62 / 100;
                    }
                    else if (Duree >= 10 && Duree < 15)
                    {
                        TauxNominal = 0.67 / 100;
                    }
                    else if (Duree >= 15 && Duree < 20)
                    {
                        TauxNominal = 0.85 / 100;
                    }
                    else if (Duree >= 20 && Duree < 25)
                    {
                        TauxNominal = 1.04 / 100;
                    }
                    else
                    {
                        TauxNominal = 1.27 / 100;
                    }
                    break;
                case TypeTauxEmprunt.TresBon:
                    if (Duree >= 7 && Duree < 10)
                    {
                        TauxNominal = 0.43 / 100;
                    }
                    else if (Duree >= 10 && Duree < 15)
                    {
                        TauxNominal = 0.55 / 100;
                    }
                    else if (Duree >= 15 && Duree < 20)
                    {
                        TauxNominal = 0.73 / 100;
                    }
                    else if (Duree >= 20 && Duree < 25)
                    {
                        TauxNominal = 0.91 / 100;
                    }
                    else
                    {
                        TauxNominal = 1.15 / 100;
                    }
                    break;
                case TypeTauxEmprunt.Excellent:
                    if (Duree >= 7 && Duree < 10)
                    {
                        TauxNominal = 0.35 / 100;
                    }
                    else if (Duree >= 10 && Duree < 15)
                    {
                        TauxNominal = 0.45 / 100;
                    }
                    else if (Duree >= 15 && Duree < 20)
                    {
                        TauxNominal = 0.58 / 100;
                    }
                    else if (Duree >= 20 && Duree < 25)
                    {
                        TauxNominal = 0.73 / 100;
                    }
                    else
                    {
                        TauxNominal = 0.89 / 100;
                    }
                    break;
            }
            return TauxNominal;
        }
        // pret differe : paye que les 
        // 1 an on ne paye que les intérets du tous les mois puis après on paye le credit normalement avec l'amortissement et tout 

        // Classe différé ? 
        // ou EstDiffere bruh

        // On rajoute quelques choses => pas avoir à modifier de test ! (uniquement si ajout de classe/changement de traitement) 
        // => boolean meilleur moyen de péter le fonctionnement privilégier les changements de classes
        // mensualité d'assurance décroit en fonction du capital restant dû 
    }
}
