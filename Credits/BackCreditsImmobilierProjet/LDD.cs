﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackCreditsImmobilierProjet
{
    public class LDD : Compte
    {
        public LDD() : base()
        {
            Taux = 0.5;
            Plafond = 12000;
        }
    }
}
