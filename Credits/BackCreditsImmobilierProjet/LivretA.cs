﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackCreditsImmobilierProjet
{
    public class LivretA : Compte
    {
        public LivretA() : base()
        {
            Taux = 0.75;
            Plafond = 22950;
        }

    }
}
