﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackCreditsImmobilierProjet
{
    public class PlanEpargneLogement : Compte
    {


        public PlanEpargneLogement() : base()
        {
            Taux = 1;
            Plafond = 61200;
        }

        public override double CalculInteret()
        {
            double res = Balance;
            for (int i = 0; i < Duree; i++)
            {
                if (PlafondEstAtteint())
                {
                    res += (Plafond * (Taux / 100));
                }
                else
                {
                    // abondement jusqu'à 10e année
                    if(i < 10) res = MontantParAbondement();
                    // intérêt jusqu'à 15e année
                    if (i < 15) res += (res * (Taux / 100));
                }
            }
            return res;
        }
    }
}
