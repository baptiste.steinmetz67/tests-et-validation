﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackCreditsImmobilierProjet
{
    public class PlanHoudini : Compte
    {
        public PlanHoudini() : base()
        {
            Taux = 8.0;
            Plafond = Double.MaxValue;
        }
    }
}
