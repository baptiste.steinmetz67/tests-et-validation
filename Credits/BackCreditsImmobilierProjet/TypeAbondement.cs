﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackCreditsImmobilierProjet
{
    public enum TypeAbondement
    {
        Aucun,
        Mensuel,
        Trimestriel,
        Annuel
    }
}
