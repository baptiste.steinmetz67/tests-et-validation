﻿using System;
using Xunit;
using BackCreditsImmobilierProjet;

namespace TestsCreditsImmobilierProjet
{
    public class CompteTest
    {
        [Fact]
        [Trait("Categorie", "Balance")]
        public void TestBalanceCompte()
        {
            Compte compte = new Compte();
            compte.Crediter(1000);
            Assert.Equal(1000, compte.Balance);
            Assert.NotEqual(-100, compte.Balance);
            Assert.NotEqual(100, compte.Balance);
            Assert.NotEqual(0, compte.Balance);
        }

        [Fact]
        [Trait("Categorie", "Abondement")]
        public void TestTypeAbondementCompte()
        {
            Compte compte = new Compte();
            Assert.Equal(TypeAbondement.Aucun, compte._typeAbondement);
            compte._typeAbondement = TypeAbondement.Mensuel;
            Assert.NotEqual(TypeAbondement.Aucun, compte._typeAbondement);
        }

        // Taux nul, après 1 an => balance = abondement.
        [Fact]
        [Trait("Categorie", "Abondement")]
        public void TestBalanceAbondementCompte()
        {
            Compte compte = new Compte();
            compte._typeAbondement = TypeAbondement.Annuel;
            compte.Abonder(100);
            compte.Duree = 1;
            compte.CalculInteret();
            Assert.Equal(compte.Abondement, compte.Balance);
            Assert.NotEqual(0, compte.Balance);
        }
    }
}
