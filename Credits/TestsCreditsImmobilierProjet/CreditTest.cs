using System;
using Xunit;
using BackCreditsImmobilierProjet;

namespace TestsCreditsImmobilierProjet
{
    public class CreditTest
    {

        [Fact]
        [Trait("Categorie", "Objet")]
        public void TestCreationCorrecte()
        {
            Credit credit = new Credit(175000);
            Assert.IsNotType<LivretA>(credit);
            Assert.IsNotType<Compte>(credit);
            Assert.IsType<Credit>(credit);
        }
        [Fact]
        [Trait("Categorie", "Taux")]
        public void TauxCorrectTest()
        {
            Credit credit = new Credit(10000);
            // Type de taux de base est "Bon"
            Assert.Equal(TypeTauxEmprunt.Bon, credit._typeTauxEmprunt);
            // S'assure que ce n'est pas un autre
            Assert.NotEqual(TypeTauxEmprunt.TresBon, credit._typeTauxEmprunt);
            Assert.NotEqual(TypeTauxEmprunt.Excellent, credit._typeTauxEmprunt);

        }

        [Fact]
        [Trait("Categorie", "TauxAssurance")]
        public void AssuranceTauxBaseTest()
        {
            Credit credit = new Credit(175000);
            
            Assert.Equal(0.003, credit.TauxAssurance);
            Assert.NotEqual(0, credit.TauxAssurance);
            Assert.NotEqual(-0.003, credit.TauxAssurance);
        }

        [Fact]
        [Trait("Categorie", "TauxAssurance")]
        public void AssuranceTauxOperationTest()
        {
            Credit credit = new Credit(175000);
            // -0.05%
            credit.EstSportif();
            Assert.Equal(0.0025, credit.TauxAssurance);
            Assert.NotEqual(0.003, credit.TauxAssurance);
            Assert.NotEqual(0, credit.TauxAssurance);
            Assert.NotEqual(-0.003, credit.TauxAssurance);
            // -0.05%
            credit.EstIngeInfo();
            Assert.Equal(0.0020, credit.TauxAssurance);
            Assert.NotEqual(0.003, credit.TauxAssurance);
            Assert.NotEqual(0.0025, credit.TauxAssurance);
            Assert.NotEqual(0, credit.TauxAssurance);
            Assert.NotEqual(-0.003, credit.TauxAssurance);
        }


        // Tests et vérification avec les premières paramètres du TP

        [Fact]
        [Trait("Categorie", "Mensualite")]
        public void CalculMensualiteAssuranceTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 25;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            credit.EstCardiaque();
            credit.EstFumeur();
            credit.EstIngeInfo();
            Assert.Equal(4.08, credit.CalculMensualiteAssurance());
            Assert.NotEqual(0, credit.CalculMensualiteAssurance());
            Assert.NotEqual(-50, credit.CalculMensualiteAssurance());
            // 4.08
        }

        [Fact]
        [Trait("Categorie", "Mensualite")]
        public void CalculMensualiteSansAssuranceTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 25;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            credit.EstCardiaque();
            credit.EstFumeur();
            credit.EstIngeInfo();
            double mensualiteSansAssurance = credit.CalculMensualiteTotale() - credit.CalculMensualiteAssurance(); 
            Assert.Equal(mensualiteSansAssurance, credit.CalculMensualiteSansAssurance());
            Assert.NotEqual(0, credit.CalculMensualiteSansAssurance());
            Assert.NotEqual(-50, credit.CalculMensualiteSansAssurance());
        }

        [Fact]
        [Trait("Categorie", "Mensualite")]
        public void CalculMensualiteTotaleTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 25;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            credit.EstCardiaque();
            credit.EstFumeur();
            credit.EstIngeInfo();

            Assert.Equal(685.22, credit.CalculMensualiteTotale());
            Assert.NotEqual(0, credit.CalculMensualiteTotale());
            Assert.NotEqual(-50, credit.CalculMensualiteTotale());
            // 685.22
        }

        [Fact]
        [Trait("Categorie", "CreditInteret")]
        public void CalculMontantTotalInteretTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 25;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            credit.EstCardiaque();
            credit.EstFumeur();
            credit.EstIngeInfo();

            Assert.Equal(30566, credit.CalculMontantTotalInteret());
            Assert.NotEqual(credit.Capital, credit.CalculMontantTotalInteret());
            Assert.NotEqual(0, credit.CalculMontantTotalInteret());
            Assert.NotEqual(-50, credit.CalculMontantTotalInteret());
            // 30566
        }

        [Fact]
        [Trait("Categorie", "CreditRemboursement")]
        public void CalculMontantTotalAssuranceTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 25;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            credit.EstCardiaque();
            credit.EstFumeur();
            credit.EstIngeInfo();

            Assert.Equal(1224, credit.CalculMontantTotalAssurance());
            Assert.NotEqual(0, credit.CalculMontantTotalAssurance());
            Assert.NotEqual(-50, credit.CalculMontantTotalAssurance());
            // 1224
        }
        [Fact]
        [Trait("Categorie", "CreditRemboursement")]
        public void MontantRembourseApresDureeTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 25;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            credit.EstCardiaque();
            credit.EstFumeur();
            credit.EstIngeInfo();

            credit.MontantRembourseApresDuree(10);
            Assert.Equal(82226.4, credit.MontantRembourseApresDuree(10));
            Assert.NotEqual(0, credit.MontantRembourseApresDuree(10));
            Assert.NotEqual(-50, credit.MontantRembourseApresDuree(10));
            // 82226.4
        }

        [Fact]
        [Trait("Categorie", "CreditRemboursement")]
        public void MontantRembourseApresDureeNegativeTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 25;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            credit.EstCardiaque();
            credit.EstFumeur();
            credit.EstIngeInfo();

            Assert.Throws<ArgumentException>(() => credit.MontantRembourseApresDuree(-10));
        }

        [Fact]
        [Trait("Categorie", "TauxNominal")]
        public void TauxNominalTest()
        {
            Credit credit = new Credit(175000);
            credit.Duree = 8;
            credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
            double taux = credit.CalculTauxEmprunt();
        }

    }
}
