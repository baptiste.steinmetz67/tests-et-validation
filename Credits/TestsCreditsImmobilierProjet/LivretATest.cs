using System;
using Xunit;
using BackCreditsImmobilierProjet;

namespace TestsCreditsImmobilierProjet
{
    public class LivretATest
    {

        [Fact]
        [Trait("Categorie", "Objet")]
        public void CreationCorrecteTest()
        {
            Compte livret = new LivretA();
            Assert.IsType<LivretA>(livret);
        }

        [Fact]
        [Trait("Categorie", "Objet")]
        public void CreationMauvaisObjetTest()
        {
            Compte livret = new LivretA();
            Assert.IsNotType<LDD>(livret);
        }

        [Fact]
        [Trait("Categorie", "Plafond")]
        public void AtteintePlafondTest()
        {
            // Abondement annuel de 100� => 134 ans avant d'atteindre le plafond
            Compte livretA = new LivretA();
            livretA._typeAbondement = TypeAbondement.Annuel;
            livretA.Abonder(100);
            double annee = livretA.CalculPlafondAnnee(livretA.Plafond);
            Assert.Equal(134, annee);
            Assert.NotEqual(0, annee);
            Assert.NotEqual(-10, annee);

            // Creditant le plafond => 0 ann�e avant d'atteindre le plafond
            Compte livretB = new LivretA();
            livretB._typeAbondement = TypeAbondement.Aucun;
            livretB.Crediter(livretB.Plafond);
            double anneeB = livretB.CalculPlafondAnnee(livretB.Plafond);
            Assert.Equal(0, anneeB);
            Assert.NotEqual(10, anneeB);
            Assert.NotEqual(-10, anneeB);

        }

        [Fact]
        [Trait("Categorie", "Interet")]
        public void CalculInteretTest()
        {
            // Balance apr�s 1 ans avec int�r�t de 0.75% => 100.75
            Compte livretA = new LivretA();
            livretA.Duree = 1;
            livretA.Crediter(100);
            livretA.CalculInteret();
            Assert.Equal(100.75, livretA.Balance);
            Assert.NotEqual(0, livretA.Balance);
            Assert.NotEqual(-10, livretA.Balance);
        }

        [Fact]
        [Trait("Categorie", "Plafond")]
        public void PlafondTest()
        {
            // Plafond est bien atteint
            Compte livretA = new LivretA();
            livretA.Crediter(livretA.Plafond);
            Assert.True(livretA.PlafondEstAtteint());

            Compte livretB = new LivretA();
            livretB.Crediter(22950);
            Assert.True(livretA.PlafondEstAtteint());
        }
    }
}
