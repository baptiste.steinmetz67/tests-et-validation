﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackCreditsImmobilierProjet;
using Xunit;

namespace TestsCreditsImmobilierProjet
{
    public class PELTest
    {
        [Fact]
        [Trait("Categorie", "Interet")]
        public void CalculInteretPELTest()
        {
            // Interet s'accumulent jusqu'à la 15e année inclus 
            // pour 15 et 16 ans même balance
            PlanEpargneLogement pel = new PlanEpargneLogement();
            pel.Crediter(100);
            pel.Duree = 16;

            PlanEpargneLogement pel2 = new PlanEpargneLogement();
            pel2.Crediter(100);
            pel2.Duree = 15;

            Assert.Equal(pel.Balance, pel2.Balance);
        }
    }
}
