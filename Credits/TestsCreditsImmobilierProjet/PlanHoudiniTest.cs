﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using BackCreditsImmobilierProjet;

namespace TestsCreditsImmobilierProjet
{
    public class PlanHoudiniTest
    {
        [Fact]
        [Trait("Categorie", "Plafond")]
        public void HoudiniPlafondJamaisAtteintTest()
        {
            PlanHoudini houdini = new PlanHoudini();
            houdini.Crediter(10000000);
            Assert.False(houdini.PlafondEstAtteint());

            houdini.Duree = 100;
            houdini.CalculInteret();

            Assert.False(houdini.PlafondEstAtteint());
        }

    }
}
