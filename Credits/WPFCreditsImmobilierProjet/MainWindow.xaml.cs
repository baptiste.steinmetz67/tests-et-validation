﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BackCreditsImmobilierProjet;
 
namespace WPFCreditsImmobilierProjet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LivretA.IsChecked = true;
            AbondementAucun.IsChecked = true;

        }

        private void Calculer_Click(object sender, RoutedEventArgs e)
        {
            Compte compte = null;
            TypeAbondement typeAbondement = TypeAbondement.Aucun;
            if (AbondementAnnuel.IsChecked.Value)
            {
                typeAbondement = TypeAbondement.Annuel;
            }
            else if (AbondementMensuel.IsChecked.Value)
            {
                typeAbondement = TypeAbondement.Mensuel;

            }
            else if (AbondementTrimestriel.IsChecked.Value)
            {
                typeAbondement = TypeAbondement.Trimestriel;

            }
            else if (AbondementAucun.IsChecked.Value)
            {
                typeAbondement = TypeAbondement.Aucun;
            }

            if (LivretA.IsChecked.Value)
            {
                compte = new LivretA();

            }
            else if (LDD.IsChecked.Value)
            {
                compte = new LDD();

            }
            else if (PlanHoudini.IsChecked.Value)
            {
                compte = new PlanHoudini();
            }
            else if (PEL.IsChecked.Value)
            {
                compte = new PlanEpargneLogement();
            }
            compte._typeAbondement = typeAbondement;
            compte.Crediter(double.Parse(MontantInitial.Text));
            compte.Abonder(double.Parse(MontantAbondement.Text)); 
            compte.Duree = double.Parse(Duree.Text); 
            ValeurResultat.Text = compte.CalculInteret().ToString();
            PlafondAtteint.Text = compte.CalculPlafondAnnee(compte.Plafond).ToString();
            ResultatCompte.Visibility = Visibility.Visible;
        }
            private void CalculerMensualite_Click(object sender, RoutedEventArgs e)
        {
            CreditResultat.Visibility = Visibility.Visible;
            Credit credit = null;
            credit = new Credit(Double.Parse(CapitalMontant.Text));
            credit.Duree = Double.Parse(DureeValeur.Text);
            switch(TauxValeur.SelectedIndex)
            {
                case 0:
                    credit._typeTauxEmprunt = TypeTauxEmprunt.Bon;
                    break;
                case 1:
                    credit._typeTauxEmprunt = TypeTauxEmprunt.TresBon;
                    break;
                case 2:
                    credit._typeTauxEmprunt = TypeTauxEmprunt.Excellent;
                    break;
            }
            if (estFumeur.SelectedIndex == 1) credit.EstFumeur();
            if (estPilote.SelectedIndex == 1) credit.EstPiloteChasse();
            if (estInge.SelectedIndex == 1) credit.EstIngeInfo();
            if (estCardiaque.SelectedIndex == 1) credit.EstCardiaque();
            if (estSportif.SelectedIndex == 1) credit.EstSportif();

            MensualiteGlobale.Text = credit.CalculMensualiteTotale().ToString();
            MensualiteAssurance.Text = credit.CalculMensualiteAssurance().ToString();
            InteretTotal.Text = credit.CalculMontantTotalAssurance().ToString();
            AssuranceTotale.Text = credit.CalculMontantTotalInteret().ToString();
            MontantRembourse.Text = credit.MontantRembourseApresDuree(10).ToString();
        }

        private void TextBoxCreditChanged(object sender, TextChangedEventArgs e)
        {
            if (CalculerMensualite == null) return;
            if (CapitalMontant.Text != "" && Double.Parse(CapitalMontant.Text) >= 50000)
            {
                CalculerMensualite.IsEnabled = true;
            }
            else
            {
                CalculerMensualite.IsEnabled = false;
            }
        }

        private void PreviewTextInputValidation(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9.-]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void DureeValeur_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CalculerMensualite == null) return;
            if(DureeValeur.Text != "" && Double.Parse(DureeValeur.Text) >= 7)
            {
                CalculerMensualite.IsEnabled = true;
            }
            else
            {
                CalculerMensualite.IsEnabled = false;

            }
        }

        private void Duree_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Calculer == null) return;
            if (DureeValeur.Text != "" && Double.Parse(Duree.Text) >= 1)
            {
                Calculer.IsEnabled = true;
            }
            else
            {
                Calculer.IsEnabled = false;

            }
        }
    }
}
