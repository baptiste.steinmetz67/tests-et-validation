﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame
{
    public class Frame
    {
        public Frame frameSuivante; 

        private List<int> lancer;

        public List<int> Lancer
        {
            get { return lancer; }
            set { lancer = value; }
        }

        private EtatFrame etat;

        public EtatFrame Etat
        {
            get { return etat; }
            set { etat = value; }

        }

        private int max;

        public int Max
        {
            get { return max; }
            set { max = value; }
        }

        public Frame()
        {
            Etat = EtatFrame.VIDE;
            Lancer = new List<int>();
            Max = 10;
        }

        public void NouveauLancer(int quilles)
        {
            Max = Max - quilles;
            if(EstNbQuilleValable(quilles))
            {
                Lancer.Add(quilles);
                ChangementEtatFrame();
            }
        }

        private bool EstNbQuilleValable(int quilles)
        {
            if (quilles < 0 || quilles > 10) 
            {
                throw new Exception("Nombres de quilles impossibles");
            }
            return true;
        }

        public int ScoreLancer()
        {
            int score = 0; 
            foreach(int lancer in Lancer)
            {
                score += lancer;
            }
            return score;
        }

        public int ScoreGlobal()
        {
            int score = ScoreLancer();
            if (EstSpare())
            {
                score += CalculSpare();
            }
            else if(EstStrike())
            {
                score += CalculStrike();
            }
            return score;
        }

        private int CalculSpare()
        {
            int score = 0;
            if(frameSuivante != null)
            {
                if(NombreDeLancerFrameSuivante() > 0)
                {
                    score += frameSuivante.Lancer[0];
                }
            }
            return score;
        }

        private int CalculStrike()
        {
            int score = 0;
            if (frameSuivante != null && NombreDeLancerFrameSuivante() > 0)
            {
                score += frameSuivante.Lancer[0];
            
                if(NombreDeLancerFrameSuivante() > 1)
                {
                    score += frameSuivante.Lancer[1];
                } 
                else if(frameSuivante.frameSuivante != null && frameSuivante.frameSuivante.Lancer.Count > 0)
                {
                    score += frameSuivante.frameSuivante.Lancer[0];
                }
            }
            return score;
        }

        private void ChangementEtatFrame()
        {
            int nombreLancer = Lancer.Count;
            if(nombreLancer == 0)
            {
                Etat = EtatFrame.VIDE;
            }
            else if(nombreLancer == 1)
            {
                ChangeEtatUnLancer();
            }
            else if(nombreLancer == 2)
            {
                ChangeEtatDeuxLancer();
                Max = 10;
            }
        }

        private bool EstSpare()
        {
            return Etat.Equals(EtatFrame.SPARE);
        }

        private bool EstStrike()
        {
            return Etat.Equals(EtatFrame.STRIKE);
        }

        private void ChangeEtatDeuxLancer()
        {
            if (ScoreLancer() == 10)
            {
                Etat = EtatFrame.SPARE;

            }
            else
            {
                Etat = EtatFrame.TROU;
            }
        }
        
        private void ChangeEtatUnLancer()
        {
            if (Lancer[0] != 10)
            {
                Etat = EtatFrame.VIDE;
            }
            else
            {
                Max = 10;
                Etat = EtatFrame.STRIKE;
            }
        }

        private int NombreDeLancerFrameSuivante()
        {
            return frameSuivante.Lancer.Count;
        }


    }
}
