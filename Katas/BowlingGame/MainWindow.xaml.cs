﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BowlingGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Partie partie;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ClickToLancerBoule_Click(object sender, RoutedEventArgs e)
        {
            if (partie == null) return;

            partie.LancerAleatoire();
            RefreshView();

        }

        private void ClickToBowling_Click(object sender, RoutedEventArgs e)
        {
            partie = null;
            FinDePartie.Text = "";
            ChangeVisibility(true);
            partie = new Partie();
            RefreshView();
        }

        private void ClickToLancerSpare_Click(object sender, RoutedEventArgs e)
        {
            if (partie == null) return;

            partie.NouveauLancer(8);
            partie.NouveauLancer(2);
            RefreshView();
        }

        private void ClickToLancerStrike_Click(object sender, RoutedEventArgs e)
        {
            if (partie == null) return;
            partie.NouveauLancer(10);
            RefreshView();
        }

        private void ChangeVisibility(bool active = false)
        {
            if(active)
            {
                BowlingGameStart.Visibility = Visibility.Visible;
                ClickToBowling.Visibility = Visibility.Hidden;
                ClickToLancerBoule.Visibility = Visibility.Visible;
                ClickToLancerSpare.Visibility = Visibility.Visible;
                ClickToLancerStrike.Visibility = Visibility.Visible;
            }
            else
            {
                ClickToBowling.Visibility = Visibility.Visible;
                ClickToLancerBoule.Visibility = Visibility.Hidden;
                ClickToLancerSpare.Visibility = Visibility.Hidden;
                ClickToLancerStrike.Visibility = Visibility.Hidden;
            }
        }


        private void RefreshView()
        {
            int score = partie.ScoreGlobal();
            int tour = partie.Tour;
            int nbTour = partie.NombreLancer;
            ScoreBowlingGlobal.Text = score.ToString();
            EtatBowlingGlobal.Text = tour.ToString();
            if (partie.FinDeLaPartie())
            {
                FinDePartie.Text = "Bien joué XxX_BowlingMaster_XxX ! A bientôt.";
                ChangeVisibility();
            }
        }
    }
}
