﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame
{
    public class Partie
    {
        public List<Frame> frames;

        public List<Frame> Frames
        {
            get { return frames; }
            set { frames = value; }
        }

        private int max;

        public int Max
        {
            get { return max; }
            set { max = value; }
        }

        private int tour;
        public int Tour
        {
            get { return tour; }
            set { tour = value; }
        }

        private int nombreLancer;
        public int NombreLancer
        {
            get { return nombreLancer; }
            set { nombreLancer = value; }
        }

        public Partie()
        {
            frames = new List<Frame>();
            frames.Add(new Frame());
            Max = 10;
            Tour = 1;
            NombreLancer = 0;
        }

        public void NouveauLancer(int quilles)
        {
            if (FinDeLaPartie())
            {
                return;
            }
            NombreLancer++;
            if (FinDeLaFrame())
            {
                Tour++;
                Frame nouvelFrame = new Frame();
                frames[frames.Count - 1].frameSuivante = nouvelFrame;
                nouvelFrame.Max = Max;
                frames.Add(nouvelFrame);
            }

            frames[frames.Count - 1].NouveauLancer(quilles);
            Max = frames[frames.Count - 1].Max;
        }

        public bool FinDeLaPartie()
        {
            return Tour == 11 || ScoreGlobal() == 300;
        }

        private bool FinDeLaFrame()
        {
            return !frames[frames.Count - 1].Etat.Equals(EtatFrame.VIDE);
        }

        public int ScoreGlobal()
        {
            int score = 0;
            foreach(Frame frame in frames)
            {
                score += frame.ScoreGlobal();
            }
            return score;
        }

        public string ChaineEtat()
        {
            return frames[frames.Count - 1].Etat.ToString();
        }


        public void LancerAleatoire()
        {
            int quilleAleatoire = RandomNumber(0, frames[frames.Count - 1].Max);
            NouveauLancer(quilleAleatoire);
        }


        // Instantiate random number generator.  
        private readonly Random _random = new Random();

        // Generates a random number within a range.      
        public int RandomNumber(int min, int max)
        {
            return _random.Next(min, max + 1);
        }

    }
}
