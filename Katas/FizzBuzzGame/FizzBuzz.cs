﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzGame
{
    public class FizzBuzz
    {
        public FizzBuzz() 
        {
        }

        public string Generer(int n)
        {
            if (n > 150 || n < 15) throw new Exception("Nombre entre 15 et 150");
            string ret = "";
            for (int i = 1; i <= n; i++)
            {
                ret = ValeurSelonMultiple(ret, i);
                if (i != n) ret += ", ";
            }
            return ret;
        }

        private static string ValeurSelonMultiple(string chaine, int incr)
        {
            if (estMultipleDe(3, incr) && estMultipleDe(5, incr))
            {
                chaine += "FizzBuzz";
            }
            else if (estMultipleDe(3, incr))
            {
                chaine += "Fizz";
            }
            else if (estMultipleDe(5, incr))
            {
                chaine += "Buzz";
            }
            else chaine += incr;
            return chaine;
        }

        private static bool estMultipleDe(int multipliant, int multiplie)
        {
            return multiplie % multipliant == 0;
        }
    }
}
