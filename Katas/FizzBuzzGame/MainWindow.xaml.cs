﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FizzBuzzGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public FizzBuzz fb;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ClickToFizzBuzz_Click(object sender, RoutedEventArgs e)
        {
            ResultatFizzBuzz.Text = "";
            FizzBuzz fb = null;
            fb = new FizzBuzz();

            int value = Int32.Parse(UserNumber.Text);
            ResultatNombre.Text = "Resultat pour " + value;

            string ret = fb.Generer(value);
            HiddenResultatFizzBuzz.Visibility = Visibility.Visible;
            ResultatFizzBuzz.Text = ret;
        }

        private void ClickToRandomFizzBuzz_Click(object sender, RoutedEventArgs e)
        {
            ResultatFizzBuzz.Text = "";
            FizzBuzz fb = null;
            fb = new FizzBuzz();
            int value = RandomNumber(15,150);
            ResultatNombre.Text = "Resultat pour " + value;

            string ret = fb.Generer(value);
            HiddenResultatFizzBuzz.Visibility = Visibility.Visible;
            ResultatFizzBuzz.Text = ret;
        }

        private void UserNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            var regex = new Regex("[0-9]");
            if (UserNumber.Text != "" && regex.IsMatch(UserNumber.Text))
            {
                int value = Int32.Parse(UserNumber.Text);
                if (value >= 15 && value <= 150)
                {
                    ResultatNombre.Text = "Resultat pour " + value;
                    ClickToFizzBuzz.IsEnabled = true;
                }
                else ClickToFizzBuzz.IsEnabled = false;
            }
        }

        // Instantiate random number generator.  
        private readonly Random _random = new Random();

        // Generates a random number within a range.      
        public int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }


    }
}
