﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TennisGame
{
    public class Joueur
    {
        public Joueur() { }
        public Joueur(string _nom)
        {
            Nom = _nom;
            Score = 0;
            Avantage = false;
        }
        private int score;
        public int Score 
        {
            get { return score; }
            set { score = value; } 
        }
        private string nom;
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        private bool avantage;
        public bool Avantage 
        {
            get { return avantage; }
            set { avantage = value; }
        }
    }
}
