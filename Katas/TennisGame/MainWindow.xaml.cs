﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TennisGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Partie tennis;
        public Joueur joueur1;
        public Joueur joueur2;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ClickToTennis_Click(object sender, RoutedEventArgs e)
        {
            tennis = null;
            joueur1 = null;
            joueur2 = null;


            joueur1 = new Joueur("Nadal");
            joueur2 = new Joueur("Federer");
            tennis = new Partie(joueur1, joueur2);
            RefreshScore();


            NomJoueur1.Text = joueur1.Nom; 
            NomJoueur2.Text = joueur2.Nom;
            JeuADebute.Visibility = Visibility.Visible;
            ClickToTennis.Visibility = Visibility.Hidden;
            ClickToJoueurJeu.Visibility = Visibility.Visible;
            VictoireJoueur.Text = "";

        }

        private void ClickToJoueurJeu_Click(object sender, RoutedEventArgs e)
        {
            int rand = RandomNumber(1, 3);
            Joueur vainqueurPoint = new Joueur();

            if (rand == 1)
            {
                vainqueurPoint = joueur1;
            }
            else
            {
                vainqueurPoint = joueur2;
            }
            if (tennis.JoueurGagneEchange(vainqueurPoint) != null)
            {
                VictoireJoueur.Text = "Victoire de " + vainqueurPoint.Nom;
                ClickToTennis.Visibility = Visibility.Visible;
                ClickToJoueurJeu.Visibility = Visibility.Hidden;
            }
            else VictoireJoueur.Text = "Point " + vainqueurPoint.Nom;

            RefreshScore();


        }


        // Instantiate random number generator.  
        private readonly Random _random = new Random();

        // Generates a random number within a range.      
        public int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }
        public void RefreshScore()
        {
            AvantageJoueur1.Text = joueur1.Avantage.ToString();
            AvantageJoueur2.Text = joueur2.Avantage.ToString();

            ScoreJoueur1.Text = joueur1.Score.ToString();
            ScoreJoueur2.Text = joueur2.Score.ToString();
        }
    }
}
