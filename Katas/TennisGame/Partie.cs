﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TennisGame
{
    public class Partie
    {
        public Partie(Joueur _joueur1, Joueur _joueur2)
        {
            joueur1 = _joueur1;
            joueur2 = _joueur2;
        }

        private Joueur joueur1;
        private Joueur joueur2;

        public Joueur Joueur1
        {
            get { return joueur1; }
            set { joueur1 = value; }
        }
        public Joueur Joueur2
        {
            get { return joueur2; }
            set { joueur2 = value; }
        }

        public Joueur JoueurGagneEchange(Joueur vainqueur)
        {
            ChangeAvantage(vainqueur);
            switch(vainqueur.Score)
            {
                case 0:
                    vainqueur.Score += 15;
                    break;
                case 15:
                    vainqueur.Score += 15;
                    break;
                case 30:
                    vainqueur.Score += 10;
                    break;
                case 40:
                    return VerificationVictoire(vainqueur);
            }
            return null;
        }

        private Joueur VerificationVictoire(Joueur vainqueur)
        {
            if (joueur1.Score == 40 && joueur2.Score != 40)
            {
                return joueur1;
            }
            if (joueur2.Score == 40 && joueur1.Score != 40)
            {
                return joueur2;
            }
            if(joueur1.Score == 40 && joueur2.Score == 40)
            {
                return VerificationApresEgalite(vainqueur);
            }
            return null;
        }


        public void ChangeAvantage(Joueur vainqueur)
        {
            if (vainqueur == joueur1)
            {
                joueur1.Avantage = true;
                joueur2.Avantage = false;
            }
            else
            {
                joueur1.Avantage = false;
                joueur2.Avantage = true;
            }
        }

        public Joueur VerificationApresEgalite(Joueur vainqueur)
        {
            if (vainqueur.Avantage)
            {
                return vainqueur;
            }
            else ChangeAvantage(vainqueur);
            return null;
        }
    }
}
