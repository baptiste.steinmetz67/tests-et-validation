﻿using System;
using Xunit;
using BowlingGame;

namespace XUnitTestBowlingGame
{
    public class TestBowlingGame
    {

        [Fact]
        [Trait("Bowling", "Frame")]
        public void TestCreationFrame()
        {
            Frame frame = new Frame();

            Assert.Throws<Exception>(() => frame.NouveauLancer(20));
            Assert.Throws<Exception>(() => frame.NouveauLancer(-5));
        }

        [Fact]
        [Trait("Bowling", "Score")]
        public void TestScore()
        {
            Frame frame = new Frame();

            frame.NouveauLancer(2);
            Assert.Equal(2, frame.ScoreLancer());
            frame.NouveauLancer(8);
            Assert.Equal(10, frame.ScoreLancer());

        }

        [Fact]
        [Trait("Bowling", "Score")]
        public void TestScoreSimple()
        {
            Partie partie = new Partie();

            int lancer = 12;
            int quilles = 1;
            for (int i = 0; i < lancer; i++)
            {
                partie.NouveauLancer(quilles);
            }
            Assert.Equal(12, partie.ScoreGlobal());
        }

        [Fact]
        [Trait("Bowling", "Score")]
        public void TestScoreSpare()
        {
            Partie partie = new Partie();

            partie.NouveauLancer(7);
            partie.NouveauLancer(3);
            partie.NouveauLancer(3);
            partie.NouveauLancer(0);
            Assert.Equal(16, partie.ScoreGlobal());
        }

        [Fact]
        [Trait("Bowling", "Strike")]
        public void TestScoreStrike()
        {
            Partie partie = new Partie();

            partie.NouveauLancer(10);
            partie.NouveauLancer(3);
            partie.NouveauLancer(7);
            Assert.Equal(30, partie.ScoreGlobal());
        }

        [Fact]
        [Trait("Bowling", "Partie")]
        public void TestScoreVarie()
        {
            Partie partie = new Partie();

            partie.NouveauLancer(10);
            partie.NouveauLancer(3);
            partie.NouveauLancer(7);
            partie.NouveauLancer(7);
            partie.NouveauLancer(0);
            partie.NouveauLancer(1);
            partie.NouveauLancer(9);
            partie.NouveauLancer(10);
            Assert.Equal(74, partie.ScoreGlobal());
        }


        [Fact]
        [Trait("Bowling", "Quille")]
        public void TestNbQuilleApresLancer()
        {
            Partie partie = new Partie();

            partie.NouveauLancer(3);
            Assert.Equal(7, partie.Max);
            partie.NouveauLancer(7);
            Assert.Equal(10, partie.Max);
            partie.NouveauLancer(10);
            Assert.Equal(10, partie.Max);
        }
    }
}
