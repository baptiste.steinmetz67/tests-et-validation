using System;
using Xunit;
using FizzBuzzGame; 

namespace XUnitTestFizzBuzzGame
{
    public class TestFizzBuzz
    {
        [Fact]
        [Trait("FizzBuzz", "Valeur")]

        public void TestValeurRetour()
        {
            FizzBuzz fizz = new FizzBuzz();
            string expectedValue = "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz";
            Assert.Equal(expectedValue, fizz.Generer(15));
        }

        [Fact]
        [Trait("FizzBuzz", "Valeur")]

        public void TestRetourString()
        {
            FizzBuzz fizz = new FizzBuzz();
            Assert.IsType<string>(fizz.Generer(18));
        }

        [Fact]
        [Trait("FizzBuzz", "Valeur")]

        public void TestExceptionValeurMinimale()
        {
            FizzBuzz fizz = new FizzBuzz();
            Assert.Throws<Exception>(() => fizz.Generer(10));
        }


        [Fact]
        [Trait("FizzBuzz", "Valeur")]

        public void TestExceptionValeurMaximale()
        {
            FizzBuzz fizz = new FizzBuzz();
            Assert.Throws<Exception>(() => fizz.Generer(152));
        }
    }
}
