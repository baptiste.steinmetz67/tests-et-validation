using System;
using Xunit;
using TennisGame; 

namespace XUnitTestTennisGame
{
    public class TestTennis
    {
        [Fact]
        [Trait("Tennis", "Avantage")]

        public void TestCreationAvantage()
        {
            Joueur joueur1 = new Joueur();
            Assert.False(joueur1.Avantage);
        }

        [Fact]
        [Trait("Tennis", "Score")]

        public void TestCreationScore()
        {
            Joueur joueur1 = new Joueur();
            Assert.Equal(0, joueur1.Score);
        }

        [Fact]
        [Trait("Tennis", "Partie")]
        public void TestCreationPartieScore()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            Assert.Equal(0, joueur1.Score);
            Assert.Equal(0, joueur2.Score);
        }

        [Fact]
        [Trait("Tennis", "Score")]
        public void TestScoreJoueurGagneJeu()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            Assert.Equal(0, joueur1.Score);

            partie.JoueurGagneEchange(joueur1);

            Assert.Equal(15, joueur1.Score);
            Assert.NotEqual(0, joueur1.Score);
            Assert.NotEqual(-15, joueur1.Score);
        }

        [Fact]
        [Trait("Tennis", "Score")]
        public void TestScoreGagneJeuDifferent()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            Assert.Equal(0, joueur1.Score);
            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur2);
            Assert.Equal(30, joueur2.Score);
            Assert.NotEqual(joueur1.Score, joueur2.Score);
        }

        [Fact]
        [Trait("Tennis", "Partie")]
        public void TestCreationPartieAvantage()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            Assert.False(joueur1.Avantage);
            Assert.False(joueur2.Avantage);
        }

        [Fact]
        [Trait("Tennis", "Victoire")]
        public void TestJoueur1Gagne()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1);
            Assert.Equal(joueur1, partie.JoueurGagneEchange(joueur1));
        }

        [Fact]
        [Trait("Tennis", "Avantage")]
        public void TestAvantage()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1); 
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur2);

            Assert.True(joueur2.Avantage);
        }

        [Fact]
        [Trait("Tennis", "Avantage")]
        public void TestVictoireApresAvantage()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur2);

            Assert.Equal(joueur2, partie.JoueurGagneEchange(joueur2));
        }

        [Fact]
        [Trait("Tennis", "Avantage")]
        public void TestPerteAvantageEgalite()
        {
            Joueur joueur2 = new Joueur();
            Joueur joueur1 = new Joueur();

            Partie partie = new Partie(joueur1, joueur2);

            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur1);
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur2);
            partie.JoueurGagneEchange(joueur1);

            Assert.True(joueur1.Avantage);
        }
    }
}
